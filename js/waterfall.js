(function (rootDiv) {
    /**
     *  Init data
     *  TODO: load data from external source
     */

    const INCREASE_STYLE = 'increase';
    const DECREASE_STYLE = 'decrease';
    const TOTAL_STYLE = 'total';

    const inputData = {
       data : [
       {
           id: "1",
           label: "January",
           value: -100,
           data: [
               {
                   id: "1.1",
                   parent: "1",
                   label: "1 week",
                   value: 20,
                   data: [
                       {
                           id: "1.1.1",
                           parent: "1.1",
                           label: "1",
                           value: 20
                       },
                       {
                           id: "1.1.2",
                           parent: "1.1",
                           label: "2",
                           value: 100
                       },
                       {
                           id: "1.1.3",
                           parent: "1.1",
                           label: "3",
                           value: -120
                       },
                       {
                           id: "1.1.4",
                           parent: "1.1",
                           label: "4",
                           value: 10
                       },
                       {
                           id: "1.1.5",
                           parent: "1.1",
                           label: "5",
                           value: 20
                       },
                       {
                           id: "1.1.6",
                           parent: "1.1",
                           label: "6",
                           value: -15
                       },
                       {
                           id: "1.1.7",
                           parent: "1.1",
                           label: "7",
                           value: 5
                       },
                       {
                           id: "1.1.8",
                           parent: "1.1",
                           label: "1 week total",
                           total: true
                       }
                   ]
               },
               {
                   id: "1.2",
                   parent: "1",
                   label: "2 week",
                   value: 10
               },
               {
                   id: "1.3",
                   parent: "1",
                   label: "3 week",
                   value: -50
               },
               {
                   id: "1.4",
                   parent: "1",
                   label: "4 week",
                   value: -80
               },
               {
                   id: "1.5",
                   parent: "1",
                   label: "January total",
                   total: true
               },
           ]
       },
       {
           id: "2",
           label: "February",
           value: 150
       },

       {
           id: "8",
           label: "July",
           value: 30
       },
       {
           id: "9",
           label: "August",
           value: -50
       },
       {
           id: "10",
           label: "September",
           value: 20
       },
       {
           id: "11",
           label: "October",
           value: 10
       },
       {
           id: "12",
           label: "November",
           value: 20
       },
       {
           id: "13",
           label: "December",
           value: 170
       },
       {
           id: "14",
           label: "Total 2",
           value: 0,
           total: true
       }

   ]
   };
    const margin = {
        left: 50,
        right: 50,
        top: 50,
        bottom: 50
    };
    const fieldSize= {
        height : 500 + margin.left + margin.right,
        width : 1100 + margin.top + margin.bottom
    };

    /**
     *
     *    Check input chart data and fix invalid values
     *
     *    @param {Array} collection - raw chart data
     *    @return {Array} collection - checked chart data
     *
     */
    function validateInputData(collection) {
        return collection.map( (element) => ({
                    label: ((element.label) ? element.label : "Value " + index),
                    value: ((element.value) ? element.value : 0),
                    data: element.data,
                    total: element.total,
                    position: { x:0, y:0 },
                    size: { height:0, width:0 }
                }));
    }


     /**
     *
     *    Find min and max chart values
     *
     *    @param {Array} collection - checked chart data
     *    @param {Number} startValue - start chart value
     *    @return {Object} range - chart extremes
     *                     range.min - minimum chart value
     *                     range.max - maximum chart value
     *
     */
    function extentChartValues(collection, startValue = 0) {
        if(!collection) {
            return { min : 0, max : 0 }
        }

        let sum = startValue;

        const range = d3.extent(collection.map( (element) => {
            sum += element.value;
            return sum;
        }));

        return {
            min: range[0],
            max: range[1]
        }
    }


    /**
     *
     *   Create scaling
     *
     */
    function getXScale(collection) {
        return d3.scaleBand()
                    .domain(collection.map( (element) => element.label ))
                    .range([0, fieldSize.width - 2 * margin.left - 2 * margin.right])
                    .paddingInner(0.5);
    }
    function getYScale(range) {
        return d3.scaleLinear()
                .domain([range.min, range.max])
                .range([fieldSize.height - 2 * margin.top - 2 * margin.bottom, 0]);
    }


    /**
     *
     *    Draw chart bars on svg element
     *
     *    @param {Object} svg - svg container
     *    @param {Array} collection - checked chart data
     *    @param {Object} range - chart extremes
     *                    range.min - minimum chart value
     *                    range.max - maximum chart value
     *    @param {Object} xScale - x scaling object
     *    @param {Function} yScale - y scaling object
     *    @return {void}
     *
     */
    function drawBars(svg, collection, range, xScale, yScale) {
        let bar = svg.selectAll("rect")
            .data(collection)
            .enter();

        let currentResult = 0;
        bar.append('rect')
            .attr('class', 'bar')

            /**
             *    Set elements position and size
             */

            .attr('x', (data, index) => {
                return xScale.step() * index;
            })
            .attr('y', (data) => {

                currentResult += data.value;

                if(data.total === true && currentResult < 0) {
                    return yScale(0);
                } else {
                    if(data.value >= 0) {
                        return yScale(currentResult);
                    } else {
                        return yScale(currentResult - data.value);
                    }
                }
            })
            .attr('width', () => {
                currentResult = 0; //FixMe: change this temp solution
                return xScale.bandwidth();
            })
            .attr('height', (data) => {
                currentResult += data.value;

                if(data.total === true) {
                    return yScale(range.max - Math.abs(currentResult)) - yScale(range.max);
                } else {
                    return yScale(range.max - Math.abs(data.value));
                }
            })

            /**
             *    Set elements style
             */

            .attr("class", (data) => {
                if(data.total) {
                    return TOTAL_STYLE;
                }

                return ((data.value >= 0) ? INCREASE_STYLE : DECREASE_STYLE);
            })

            /**
             *    Set elements events
             *    TODO: display data in box
             */

            .on('click', function (data, index) {

                const elements = [
                        {
                            className: "icon-close",
                            action: function (element) {
                                element.remove();
                            }
                        },
                        {
                            className: "icon-trash",
                            action: function () {
                                console.log('Undetected button has been clicked!');
                            }
                        },
                        {
                            className: "icon-one-finger",
                            action: function () {
                                console.log('Finger button has been clicked!');
                            }
                        }
                    ];
                svg.call(drawMenu, elements, xScale, yScale, {
                    x: this.getAttribute('x'),
                    y: this.getAttribute('y'),
                    height: this.getAttribute('height'),
                    width: this.getAttribute('width'),
                    margin: {
                        top: 20,
                        right: 10,
                        bottom: 20,
                        left: 10
                    }
                });

                this.parentElement.appendChild(this);
            });
    }


    /**
     *
     *    Draw axis on svg element
     *
     *    @param {Object} svg - svg container
     *    @param {Array} collection - checked chart data
     *    @param {Object} xScale - x scaling object
     *    @param {Function} yScale - y scaling object
     *    @return {void}
     *
     */
    function drawAxis(svg, collection, xScale, yScale) {
        const xAxis = d3.axisBottom()
            .scale(xScale)
            .ticks(collection.length, d3.format(",d"));

        const yAxis = d3.axisLeft()
            .scale(yScale);

        // X axis
        svg.append("g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + yScale(0) + ")")
            .call(xAxis);

        // Y axis
        svg.append("g")
            .attr("class", "y axis")
            .call(yAxis);
    }

    function drawMenu (svg, elements, xScale, yScale, config) {
        const { x = xScale(0), y = yScale(0), height = 80, width = 80, margin = {} } = config;
        const { top = 10, bottom = 10, left = 20, right = 20 } = margin;

        const menu = {
            x: Number(x) - Number(left),
            y: Number(y) - Number(top),
            height: Number(height) + Number(top) + Number(bottom),
            width: Number(width) + Number(left) + Number(right)
        };

        svg.selectAll('#chartBarMenu').remove();
        const container = svg.append('g').attr('id', 'chartBarMenu');

        //create blur filter
        const defs = container.append("defs");
        const filter = defs.append("filter")
                           .attr("id", "blur");
        filter.append("feGaussianBlur")
              .attr('in', 'SourceGraphic')
              .attr('stdDeviation', '3');


        container.append('rect')
                 .attr('class', 'chartBarMenu')
                 .attr('x', menu.x)
                 .attr('y', menu.y)
                 .attr('height', menu.height)
                 .attr('width', menu.width);


        let submenuHeight = menu.height;
        if(menu.height < elements.length * 22 + 6) {
            submenuHeight = elements.length * 22 + 6;
        }

        container.append('rect')
                 .attr('fill', '#3c3c3c')
                 .attr('x', menu.x + menu.width - 1)
                 .attr('y', menu.y + 2)
                 .attr('height', submenuHeight - 2)
                 .attr('width', menu.width/3 - 2)
                 .attr('filter', 'url(#blur)');
        container.append('rect')
                 .attr('class', 'chartBarSubMenu')
                 .attr('x', menu.x + menu.width)
                 .attr('y', menu.y)
                 .attr('height', submenuHeight)
                 .attr('width', menu.width / 3);


        container.call(drawMenuElements, elements, menu.x + menu.width + (menu.width/3 - 16)/2, menu.y);
    }

    function drawMenuElements(svg, elements, x, y) {
        let menu = svg.selectAll('option')
            .data(elements)
            .enter();

        menu.append('svg:foreignObject')
            .attr('width', 20)
            .attr('height', 20)
            .attr('x', x)
            .attr('y', (data, index) => {
                return y + (22 * index + 6);
            })
            .append('xhtml:span')
            .attr('class', data => data.className)
            .on('click', function (data) {
                data.action(this.parentElement.parentElement);
            })
    }


    /**
     *
     *    Draw axis, chart bars and navigation button on svg element
     *
     *    @param {Object} svg - svg container
     *    @param {Array} collection - checked collection
     *    @return {void}
     *
     */
    function drawChart(svg, collection) {

        let chartData = validateInputData(collection);
        let yRange = extentChartValues(chartData);

        let xScale = getXScale(chartData);
        let yScale = getYScale(yRange);

        svg.append('g')
            .call(drawBars, chartData, yRange, xScale, yScale);

        svg.call(drawAxis, chartData, xScale, yScale);
    }





    //
    // Entry point
    //
    //


    let svg = d3.select(rootDiv)
        .append("svg")
        .attr("width", fieldSize.width - margin.left - margin.right)
        .attr("height", fieldSize.height - margin.top - margin.bottom)
        .append("g")
        .attr("transform", 'translate(' + margin.left +', ' + margin.top +')')
        .call(drawChart, inputData.data);


    // TODO: get parent width and height using d3
    // TODO: onResize


})("#waterfallRoot");