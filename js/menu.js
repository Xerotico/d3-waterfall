(function () {
    const menuContainer = document.getElementsByClassName('menuContainer')[0];
    const leftMenuElements = document.getElementsByClassName('elements')[0].getElementsByClassName('element');
    const tabs = document.getElementsByClassName('tabs')[0].getElementsByTagName('label');

    for(let i=0; i<tabs.length; i++) {
        tabs[i].onclick = () => {
            for(let j=0; j<tabs.length; j++) {
                tabs[j].className = '';
            }

            tabs[i].className = 'active';
        };
    }

    for(let i=0; i<leftMenuElements.length; i++) {
        leftMenuElements[i].onclick = openSubMenu;
    };


    function openMenu() {
        menuContainer.className = 'menuContainer opened';
    }
    function closeMenu() {
        menuContainer.className = 'menuContainer closed';
    }
    function openSubMenu() {
        menuContainer.className = 'menuContainer subMenuOpened';
    }

    buttonOpenMenu.onclick = openMenu;
    buttonCloseMenu.onclick = closeMenu;
})();